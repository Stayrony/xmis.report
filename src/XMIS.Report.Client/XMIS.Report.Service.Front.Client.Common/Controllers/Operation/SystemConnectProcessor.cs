﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemConnectProcessor.cs" company="">
//   
// </copyright>
// <summary>
//   The system connect processor.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

#region

using System;
using Microsoft.AspNet.SignalR.Client;
using XMIS.Report.Service.Front.Client.Common.Context;

#endregion

namespace XMIS.Report.Service.Front.Client.Common.Controllers.Operation
{
    /// <summary>
    ///     The system connect processor.
    /// </summary>
    public class SystemConnectProcessor
    {
        /// <summary>
        ///     The error.
        /// </summary>
        private string error = string.Empty;

        /// <summary>
        ///     Gets or sets the error.
        /// </summary>
        public string Error
        {
            get { return error; }
            set { error = value; }
        }

        /// <summary>
        /// The connect.
        /// </summary>
        /// <param name="serviceUrl">
        /// The service url.
        /// </param>
        public void Connect(string serviceUrl)
        {
            try
            {
                var hubConn = new HubConnection(serviceUrl);
                hubConn.Error += HubConnOnError;
                var reportHubProxy = hubConn.CreateHubProxy("reportHub");
                ServiceContext.HubConnection = hubConn;
                ServiceContext.ReportProxy = reportHubProxy;

                hubConn.Start().Wait();
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                Console.WriteLine("Error: " + Error);
                throw;
            }
        }

        /// <summary>
        /// The hub conn on error.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        private void HubConnOnError(Exception exception)
        {
            Disconnect();
        }

        /// <summary>
        ///     The disconnect.
        /// </summary>
        public void Disconnect()
        {
            if (ServiceContext.HubConnection != null)
            {
                if (ServiceContext.HubConnection.State != ConnectionState.Disconnected)
                {
                    ServiceContext.HubConnection.Stop();
                }
            }
        }
    }
}